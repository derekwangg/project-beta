from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from datetime import date, time

# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model=Technician
    properties=["name", "employee_number", "id"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
                "vin",
                "color",
                "year",
                "import_href"
                ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ["vip", "vin", "customer_name", "date", "time", "reason", "completed", "id"]
    def get_extra_data(self,o):
        return {"technician": o.technician.name}


@require_http_methods(["GET","POST"])
def api_list_technicians(request):
    if request.method== "GET":
        technicians=Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        content=json.loads(request.body)
        technician=Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, automobile_vo_vin = None):
    if request.method =="GET":
        if automobile_vo_vin is not None:
            appointment=Appointment.objects.filter(vin=automobile_vo_vin)
        else:
            appointment=Appointment.objects.all()
        return JsonResponse({"appointments": appointment}, encoder=AppointmentEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            technician=Technician.objects.get(name=content["technician"])
            content["technician"]= technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=400,
            )
        try:
            automobile=AutomobileVO.objects.get(vin=content["vin"])
            if content["vin"]==automobile.vin:
                content["vip"] = True
                appointment=Appointment.objects.create(**content)
                return JsonResponse(
                    appointment,
                    encoder=AppointmentEncoder,
                    safe=False
                )
        except AutomobileVO.DoesNotExist:
            appointment=Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )


@require_http_methods(["DELETE", "PUT"])
def api_update_appointment(request, id):
    if request.method =="DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content=json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        appointment=Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
