from django.db import models
from datetime import datetime
# Create your models here.

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Technician(models.Model):
    name=models.CharField(max_length=200)
    employee_number=models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name=models.CharField(max_length=200)
    date=models.DateField(null=True)
    time=models.TimeField(null=True)
    reason=models.CharField(max_length=200)
    technician= models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE
    )
    vip=models.BooleanField(default=False)
    completed=models.BooleanField(default=False)
