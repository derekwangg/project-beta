from django.urls import path

from .views import api_list_sales, api_list_customers, api_list_salesperson, api_show_salesperson,api_show_customer, api_show_saleshistory, api_list_autos

urlpatterns = [
    

    path("customer/", api_list_customers, name="api_create_customers"),
    path("customer/<int:id>/", api_show_customer, name="api_show_customer"),
    path("salesperson/", api_list_salesperson, name="api_create_salesperson"),
    path("salesperson/<int:id>/", api_show_salesperson, name="api_show_salesperson"),
    path("sales/", api_list_sales, name="api_create_sales"),
    path("salesperson/<int:id>/saleshistory/", api_show_saleshistory, name="api_show_saleshistory"),
    path("autos/", api_list_autos, name="api_list_autos"),
]


