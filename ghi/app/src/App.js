import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ManufacturerList from './Inventory/ManufacturerList';
import ModelForm from './Inventory/ModelForm';
import Nav from './Nav';
import ModelList from './Inventory/ModelList';
import AutomobileForm from './Inventory/AutomobileForm';
import AutomobileList from './Inventory/AutomobileList';
import TechnicianForm from './Services/TechnicianForm';
import AppointmentForm from './Services/AppointmentForm';
import AppointmentList from './Services/AppointmentList';
import SalespersonForm from './sales/SalesPersonForm';
import CustomerForm from './sales/CustomerForm';
import SalesForm from './sales/SaleForm';
import SalesList from './sales/ListSales';
import SalesHistory from './sales/ListSalesHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList/>}/>
            <Route path="new" element={<ManufacturerForm/>}/>
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList/>}/>
            <Route path="new" element={<ModelForm/>}/>
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
            <Route path="" element={<AutomobileList />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm/>}/>
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
          </Route>
          <Route path="salesperson">
            <Route path="new" element={<SalespersonForm />} />
            <Route path="saleshistory" element={<SalesHistory />} />
          </Route>
          <Route path="customer">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SalesForm />} />
            <Route path="" element={<SalesList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
