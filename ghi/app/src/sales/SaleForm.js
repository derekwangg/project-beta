import React, { useEffect, useState } from 'react';



const SalesForm = () => {
    const [autos, setAutos] = useState([]);
    const [salespersons, setSalesPersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [auto, setAuto] = useState('');
    const [salesperson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }


    const handleAutoChange = (event) => {
        const value = event.target.value
        setAuto(value);
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value);
    }
    const handlePriceChange = (event) => {

        const value = parseFloat(event.target.value);
        
        setPrice(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value);
    }

    const resetState = () => {
        setAuto('');
        setSalesPerson('');
        setPrice('');
        setCustomer('');
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

            data.auto = auto;
            data.sales_person = salesperson;
            data.customer = customer;
            data.price = price;


        const saleUrl = `http://localhost:8090/api/sales/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const saleResponse = await fetch(saleUrl, fetchConfig)
        if (saleResponse.ok) {
          setShowSuccessMessage(true);
          setFormSubmitted(true);
          resetState();
          
        } else if (!saleResponse.ok) {
            
            resetState();
        }
    }
    

    const fetchAuto = async () => {
    const autoUrl = 'http://localhost:8090/api/autos/';

    const autoResponse = await fetch(autoUrl);

    if (autoResponse.ok) {
      const data = await autoResponse.json();
      setAutos(data.autos)
      
      }
    }

    const fetchSalesperson = async () => {
    const salesPersonUrl = 'http://localhost:8090/api/salesperson/';

    const SalesPersonresponse = await fetch(salesPersonUrl);

    if (SalesPersonresponse.ok) {
        const data = await SalesPersonresponse.json();
        setSalesPersons(data.salespersons)
        
        }
    }

    const fetchCustomer = async () => {
    const customerUrl = 'http://localhost:8090/api/customer/';

    const customerResponse = await fetch(customerUrl);

    if (customerResponse.ok) {
        const data = await customerResponse.json();
        setCustomers(data.customers)
        
        }
    }

    

  useEffect(() => {
    fetchAuto(); fetchSalesperson(); fetchCustomer();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form" className={formClasses}>
              <div>
                <select placeholder="Choose an Automobile" onChange={handleAutoChange} value={auto} required name="auto" id="auto" className="form-select mb-3">
                <option value="">Choose an Automobile</option>
                  {autos.filter(auto=>!auto.sold).map(auto => {
                    return (
                        <option value={auto.vin} key={auto.id}>
                            {auto.vin}
                        </option>
                    );
                  })}
                </select>
                <select placeholder="Choose an Sales Person" onChange={handleSalesPersonChange} value={salesperson} required name="sales_person" id="sales_person" className="form-select mb-3">
                <option value="">Choose a Sales Person</option>
                    {salespersons.map(salesperson => {
                    return (
                        <option value={salesperson.name} key={salesperson.name}>
                            {salesperson.name}
                        </option>
                    );
                  })}
                </select>
                <select placeholder="Choose an Customer" onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select mb-3">
                <option value="">Choose a Customer</option>
                    {customers.map(customer => {
                    return (
                        <option value={customer.name} key={customer.name}>
                            {customer.name}
                        </option>
                    );
                  })}
                </select>
                <div>
                </div>
                <div className="mb-3 form-outline">
                    <input min= "0" step="0.01" onChange={handlePriceChange} value={price} placeholder="Sale Price" required type="number" name="price" id="price" className="form-control"/>
                    <label htmlFor="price"></label>
                </div>
              </div>
              <button className="btn btn-primary mt-1">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
              </div>
          </div>
        </div>
      </div>
  )
}

export default SalesForm;