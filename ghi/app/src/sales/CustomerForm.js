import React, { useState } from 'react';

const CustomerForm = () => {
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }


    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value);
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value);
    }


    const resetState = () => {
        setName('');
        setAddress('');
        setPhoneNumber('');

    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber;
        


        const customerUrl = `http://localhost:8090/api/customer/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
          const newCustomer = await response.json();
          setShowSuccessMessage(true);
          setFormSubmitted(true);


          resetState();
        }
    }
    


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Potential Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form" className={formClasses}>
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" name="address" id="address" className="form-control"/>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
              </div>
          </div>
        </div>
      </div>
  )
}

export default CustomerForm;