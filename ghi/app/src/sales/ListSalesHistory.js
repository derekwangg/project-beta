import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';


function SalesHistory() {

    const [sales, setSales] = useState([]);
    const [salesperson, setSalesPerson] = useState('');
    const [salespersons, setSalesPersons] = useState([]);

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value);
    }

    const handleSalesChange = (event) => {
        const value = event.target.value
        setSales(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }


    const fetchSalesperson = async () => {
    const salesPersonUrl = 'http://localhost:8090/api/salesperson/';

    const SalesPersonresponse = await fetch(salesPersonUrl);

    if (SalesPersonresponse.ok) {
        const data = await SalesPersonresponse.json();
        setSalesPersons(data.salespersons)
        
        }
    }

    useEffect(() => {
    fetchData(); fetchSalesperson();

    }, []);

    return (
    <div className="container">
        <h1>History of sales record for a Sales Person</h1>
        <select placeholder="Choose an Sales Person" onChange={handleSalesPersonChange} value={salesperson} required name="sales_person" id="sales_person" className="form-select mb-3">
            <option value="">Choose a Sales Person</option>
                {salespersons.map(salesperson => {
                return (
                    <option value={salesperson.name} key={salesperson.name}>
                        {salesperson.name}
                    </option>
                );
                })}
        </select>
        <div className="row">
            <div className="col-sm">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Sale Person</th>
                    <th>Customer</th>
                    <th>Vin</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                {sales.filter((sale) => sale.sales_person.includes(salesperson)).map((sale) => {
                    return (
                    <tr key={sale.id}>
                        <td>{ sale.sales_person }</td>
                        <td>{sale.customer}</td>
                        <td>{sale.auto}</td>
                        <td>{sale.price}</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            </div>
        </div>
        <Link to="/sales/new" className="mt-3 btn btn-primary">Add a sales record</Link>
    </div>
    );
    }
export default SalesHistory;