import React, { useState } from 'react';

const SalespersonForm = () => {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value);
    }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value);
    }


    const resetState = () => {
        setName('');
        setEmployeeNumber('');

    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.employee_number = employeeNumber;
        


        const salespersonUrl = `http://localhost:8090/api/salesperson/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch(salespersonUrl, fetchConfig)
        if (response.ok) {
          const newSalesperson = await response.json();
          setShowSuccessMessage(true);
          setFormSubmitted(true);

          resetState();
        }
    }
    


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Sales Person</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form" className={formClasses}>
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="employee_number" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
              </div>
          </div>
        </div>
      </div>
  )
}

export default SalespersonForm;