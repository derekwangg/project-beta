import React, { useState } from 'react';

function TechnicianForm() {
    const [showSuccessMessage, setShowSuccessMessage]=useState(false)
    const [formSubmitted, setFormSubmitted]= useState(false)
    const [formData, setFormData] = useState({
        name: '',
        employee_number: ''
    })
    let formClasses='';
    if (formSubmitted && showSuccessMessage){
        formClasses = 'd-none'
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8080/api/technicians/`;

        const fetchConfig = {
            method: "POST",

            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {

        setShowSuccessMessage(true)
        setFormSubmitted(true)
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;


        setFormData({
        ...formData,

        [inputName]: value
        });
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a technician</h1>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
            </div>
            <form onSubmit={handleSubmit} id="create-technician-form" className={formClasses}>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name}placeholder="Name" required type="text" name="name" id="ame" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.employee_number}placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}


export default TechnicianForm;
