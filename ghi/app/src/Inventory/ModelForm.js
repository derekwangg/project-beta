import React, { useEffect, useState } from 'react';

function ModelForm() {
    const [manufacturers, setManufacturers] = useState([])

    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/models/`;

        const fetchConfig = {
            method: "post",

            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        setShowSuccessMessage(true);
        setFormSubmitted(true);
        setFormData({
            name: '',
            picture_url: '',
            manufacturer_id: '',
        });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;


        setFormData({
        ...formData,

        [inputName]: value
        });
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-model-form" className={formClasses}>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name}placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.picture_url}placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="mb-3">
                <select onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select" value={formData.manufacturer_id}>
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
              </div>
            </div>
        </div>
        </div>
    )
    }


export default ModelForm;
