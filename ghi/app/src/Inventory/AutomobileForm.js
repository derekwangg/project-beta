import React, { useEffect, useState } from 'react';

const AutomobileForm = props => {
    const [models, setModels] = useState([]);
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [color, setColor] = useState('');

    const [model, setModel] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);

    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }

    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value);
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value);
    }

    const resetState = () => {
        setModel('');
        setYear('');
        setColor('');
        setVin('');
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;


        const automobileUrl = `http://localhost:8100/api/automobiles/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch(automobileUrl, fetchConfig)
        if (response.ok) {
          const newAutomobile = await response.json();
          setShowSuccessMessage(true);
          setFormSubmitted(true);
          resetState();
        }
    }
    

    const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
      
      }
    }
    

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form" className={formClasses}>
            <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year}placeholder="year" required type="text" name="year" id="year" className="form-control"/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input maxLength="17" onChange={handleVinChange} value={vin}placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin</label>

              </div>
              <div className="mb-3">
                <select onChange={handleModelChange} value={model} required name="model" id="model" className="form-select">
                <option value="">Choose a model</option>
                    {models.map(model => {
                    return (
                        <option value={model.id} key={model.id}>
                            {model.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Form submitted
              </div>
          </div>
        </div>
      </div>
  )
}

export default AutomobileForm;