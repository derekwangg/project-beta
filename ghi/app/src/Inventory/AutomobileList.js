import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function AutomobileList() {
    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

  useEffect(() => {
    fetchData();
  }, []);

    return (
    <div className="container">
        <h1>List of Automobiles</h1>
        <div className="row">
            <div className="col-sm">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
                </thead>
                <tbody>
                {autos.map(auto => {
                    return (
                    <tr key={auto.id}>
                        <td>{ auto.vin }</td>
                        <td>{ auto.color }</td>
                        <td>{ auto.year }</td>
                        <td>{ auto.model.name }</td>
                        <td>{ auto.model.manufacturer.name}</td>
                        
                    </tr>
                    );
                })}
                </tbody>
            </table>
            </div>
        </div>
        <Link to="/automobiles/new" className="mt-3 btn btn-primary">Add an automobile</Link>
    </div>
    );
  }

export default AutomobileList;
