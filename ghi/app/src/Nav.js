import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Inventory
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" aria-current="page" to="/manufacturers">List Manufacturers</NavLink>
                  <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new">Create a Manufacturer</NavLink>
                  <div className="dropdown-divider"></div>
                  <NavLink className="dropdown-item" to="/models/">List Vehicle Models</NavLink>
                  <NavLink className="dropdown-item" aria-current="page" to="/models/new">Create a Vehicle Model</NavLink>
                  <div className="dropdown-divider"></div>
                  <NavLink className="dropdown-item" to="/automobiles/">List Automobile</NavLink>
                  <NavLink className="dropdown-item" to="/automobiles/new">Create Automobile</NavLink>
                </div>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Services
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <NavLink className="dropdown-item" aria-current="page" to="/technicians/new">Create a technician</NavLink>
                  <div className="dropdown-divider"></div>
                  <NavLink className="dropdown-item" to="/appointments/">List Appointments</NavLink>
                  <NavLink className="dropdown-item" aria-current="page" to="/appointments/new">Create a service appointment</NavLink>
                </div>
                </li>
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="SalesnavbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Sales
                </a>
                <div className="dropdown-menu" aria-labelledby="SalesnavbarDropdown">
                  <NavLink className="dropdown-item" aria-current="page" to="/salesperson/saleshistory">Sale Person's Sale History</NavLink>
                  <NavLink className="dropdown-item" aria-current="page" to="/salesperson/new">Add a Sales Person</NavLink>
                  <div className="dropdown-divider"></div>
                  <NavLink className="dropdown-item" aria-current="page" to="/customer/new">Add a Potential Customer</NavLink>
                  <div className="dropdown-divider"></div>
                  <NavLink className="dropdown-item" to="/sales/">List Sales Record</NavLink>
                  <NavLink className="dropdown-item" to="/sales/new">Create Sale Record</NavLink>
                </div>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
